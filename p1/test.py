import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import utils
import numpy as np

img = mpimg.imread('test.jpg')
gray =  utils.rgb2gray(img)
hist = utils.hist(gray)
totalPixels = len(hist)
cumHist = utils.CumulativeHist(gray)

for x in range (0, 256):
    cumHist[x] = np.true_divide(cumHist[x],totalPixels)


#Settiung up plot distribution:
fig, ((image, histogram), (imageEc, histogramEc)) = plt.subplots(nrows=2, ncols=2)

#Image
image.imshow(gray, cmap='gray', vmin = 0, vmax = 255)

#Histogram
color = 'tab:red'
histogram.set_xlabel('Histogram')
histogram.set_ylabel('density', color = color)
histogram.hist(hist, bins=256, color = color)
histogram.tick_params(axis='y', color= color)

#Cummulative Histogram
cumulativeHistogram = histogram.twinx()
color = 'tab:blue'
cumulativeHistogram.set_ylabel('percentage %', color = color)
cumulativeHistogram.plot(cumHist, 'b--')
cumulativeHistogram.tick_params(axis='y', labelcolor = color)

###################################################################

#Ecualization
gray = utils.equalice(gray)
hist = utils.hist(gray)
cumHist = utils.CumulativeHist(gray)

for x in range (0, 256):
    cumHist[x] = np.true_divide(cumHist[x],totalPixels)

#Image
imageEc.imshow(gray, cmap='gray', vmin = 0, vmax = 255)

#Histogram
color = 'tab:red'
histogramEc.set_xlabel('Equalized Histogram')
histogramEc.set_ylabel('density', color = color)
histogramEc.hist(hist, bins=256, color = color)
histogramEc.tick_params(axis='y', color= color)

#Cummulative Histogram
cumulativeHistogramEc = histogramEc.twinx()
color = 'tab:blue'
cumulativeHistogramEc.set_ylabel('percentage %', color = color)
cumulativeHistogramEc.plot(cumHist, color = color)
cumulativeHistogramEc.tick_params(axis='y', labelcolor = color)


fig.tight_layout()

#utils.exploreImage(gray, (5,3))

#Plot
plt.show()