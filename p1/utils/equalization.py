import numpy as np
from .hist import CumulativeHist
from .hist import hist

def equalice(image):
    hist = CumulativeHist(image)
    totalPixels = len(image) * len(image[0])

    r = 0
    c = 0
    for row in image:
        for col in row:
            image[r][c] = np.divide(hist[col]*255,totalPixels)
            c += 1
        c = 0
        r+= 1

    return image
