import numpy as np

def rgb2gray(image):

    gray = np.dot(image[...,:3], [0.2989, 0.5870, 0.1140])
    return np.rint(gray).astype(int)