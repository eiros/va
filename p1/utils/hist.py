import numpy as np

def hist(image):
    hist = []

    hist = image.flatten()

#    for row in image:
#        for col in row:
#            hist.append(col)

    return hist

def CumulativeHist(image):
    hist = [0] * 256

    for row in image:
        for col in row:
            hist[col] += 1
    
    for x in range (1, 256):
        hist[x] += hist[x-1]

    return hist